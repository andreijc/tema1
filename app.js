var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

const fs = require('fs');
const os = require('os');
const axios = require('axios');
const lodash = require('lodash');
const {v4:uuidv4} = require('uuid');
const Employee = require('./employee.js');

//console.log(`${os.tmpdir()}\\employees.json`);

const GetFileFromUrl = async(url) =>{
  const response = await axios.get(url);
  return response.data;
}

const ValidateFile = (file) =>{

  return file.filter( (empl) =>{
    return (empl.hasOwnProperty('id') && empl.id > 0) &&

           (empl.hasOwnProperty('name') && lodash.isString(empl.name) && 
           lodash.size(empl.name) >= 2 && lodash.size(empl.name) <= 64) &&

           (empl.hasOwnProperty('age') && empl.age > 0) &&

           (empl.hasOwnProperty('abilities') && lodash.isArray(empl.abilities) &&
           lodash.every(empl.abilities,(item) =>{
             return lodash.size(item) >= 2 && lodash.size(item) <= 64})) &&

           (empl.hasOwnProperty('stamina') && empl.stamina >= 1 && empl.stamina <= 100);
          });
} 

const WriteToTemp = async (file) =>{
  
  return new Promise((resolve,reject)=>{
    const data = JSON.stringify(file);
    fs.writeFile(`${os.tmpdir()}\\employees.json`,data,(err)=>{
      console.log('in promise tmp');
      resolve('succes tmp');
    });
  });

}

const GetEmployeesByStamina = (employees,stamina) =>{
  
  return employees.filter((empl) =>{
    return empl.stamina > stamina;
  }).sort((a,b)=>{
    return a.stamina-b.stamina;
  });

}

const GetEmployeesByAbility = (employees,ability) =>{

  return employees.filter((empl) =>{
    return empl.abilities.includes(ability);
  });

}

const GetEmployeesTotalAge = (employees) =>{

  return employees.reduce((prev,current)=>{
    return ({age:prev.age + current.age});
  });

}

const main = async() =>{
  const addr = 'https://codezilla-assets.s3.eu-central-1.amazonaws.com/employees.json';
  let employees_file;
  let employees = [];

  employees_file = await GetFileFromUrl(addr);

  let tmp = ValidateFile(employees_file);

  tmp.map( (empl) =>{
    employees.push(new Employee(empl.id,empl.name,empl.age,empl.abilities,empl.stamina));
  });


  await WriteToTemp(employees_file);

  console.log(GetEmployeesByStamina(employees,75));
  console.log(GetEmployeesByAbility(employees,'leadership'));
  console.log(GetEmployeesTotalAge(employees).age);


  //employees = employees.map(obj=>({...obj,internal_id:uuidv4()}));
  employees = employees.map((obj)=>{
    obj.internal_id = uuidv4();
    return obj;
  });

  console.log(employees);

  
  //Returneaza un array de rezultate in ordinea pasarii promisurilor,indiferent de ordinea executarii lor
  console.log(await Promise.all(employees.map((obj)=>{return obj.WriteEmployee();})));
  //Intoarce rezultatul primului promise exectutat cu succes
  console.log(await Promise.race(employees.map((obj)=>{return obj.WriteEmployee();})));


}

main();
