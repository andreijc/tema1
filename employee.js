const fs = require('fs');

class Employee{

    constructor(id,name,age,abilities,stamina){
        this.id = id;
        this.name = name;
        this.age = age;
        this.abilities = abilities;
        this.stamina = stamina;

    }

    WriteEmployee = async () =>{

        return new Promise((resolve,reject)=>{
            const data = JSON.stringify(this);
            fs.writeFile(`${this.id}.json`,data,(err)=>{
              console.log(`log${this.id}`);
              resolve(`succes${this.id}`);
            });
          });
          
    }
}

module.exports = Employee;